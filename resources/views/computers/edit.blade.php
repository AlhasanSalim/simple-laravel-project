@extends('layout')
@section('title', 'Edit Computers')

@section('content')
    <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
        <div class="flex justify-center pt-8">
            <h1>Edit an old Computer</h1>
        </div>
        <div class="flex justify-center">
            <form class="smooth" action="{{route('computers.update', ['computer' => $computer->id])}}" method="POST">
                @csrf
                @method('PUT')
                <div>
                    <label for="computer-name">Computer Name</label>
                    <input value="{{$computer -> name}}" type="text" name="computer-name" id="computer-name">
                    @error('computer-name')
                        <div class="form-error">
                            {{$message}}
                        </div>
                    @enderror
                </div>


                <div>
                    <label for="computer-origin">Computer Origin</label>
                    <input value="{{$computer->origin}}" type="text" name="computer-origin" id="computer-origin" >
                    @error('computer-origin')
                        <div class="form-error">
                            {{$message}}
                        </div>
                    @enderror
                </div>


                <div>
                    <label for="computer-price">Computer Price</label>
                    <input value="{{$computer->price}}" type="text" name="computer-price" id="computer-price">
                    @error('computer-price')
                        <div class="form-error">
                            {{$message}}
                        </div>
                    @enderror
                </div>
                <div>
                    <button type="submit">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection