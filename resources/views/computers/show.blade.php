@extends('layout')
@section('title', 'Show Computers')

@section('content')
    <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
        <div class="flex justify-center pt-8">
            <h1>COMPUTERS</h1>
        </div>
        <div class="mt-8">
            <h3>{{$computer['name']}} ({{$computer['origin']}}) - <strong>{{$computer['price']}}$</strong></h3>
        </div>
        <a class="edit-btn" href="{{ route('computers.edit', $computer->id) }}">Edit</a>
    
        <form method="POST" action="{{ route('computers.destroy', $computer->id) }}">
            @csrf
            @method('DELETE')
            <input class="delete-btn" value="Delete" type="submit">
        </form>
    </div>
@endsection