@extends('layout')
@section('title', 'Create Computers')

@section('content')
    <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
        <div class="flex justify-center pt-8">
            <h1>Create a new Computer</h1>
        </div>
        <div class="flex justify-center">
            <form class="smooth" action="{{route('computers.store')}}" method="post">
                @csrf
                <div>
                    <label for="computer-name">Computer Name</label>
                    <input value="{{old('computer-name')}}" type="text" name="computer-name" id="computer-name">
                    @error('computer-name')
                        <div class="form-error">
                            {{$message}}
                        </div>
                    @enderror
                </div>


                <div>
                    <label for="computer-origin">Computer Origin</label>
                    <input value="{{old('computer-origin')}}" type="text" name="computer-origin" id="computer-origin" >
                    @error('computer-origin')
                        <div class="form-error">
                            {{$message}}
                        </div>
                    @enderror
                </div>


                <div>
                    <label for="computer-price">Computer Price</label>
                    <input value="{{old('computer-price')}}" type="text" name="computer-price" id="computer-price">
                    @error('computer-price')
                        <div class="form-error">
                            {{$message}}
                        </div>
                    @enderror
                </div>
                <div>
                    <button type="submit">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection